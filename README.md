A preocupação com a segurança do trabalho e o conforto ambiental nas empresas tem sido um assunto de relevância dentre trabalhadores, empresários, consumidores, poder
público e sociedade em geral Essa crescente preocupação prevê adequação para todos os setores produtivos do país, em especial ao segmento do agronegócio.

Levando este fator em consideração as questões referentes a segurança do trabalho, especialmente na questão da acústica das edificações agroindustriais, o presente
trabalho tem como objetivo elaborar o mapa de ruído ambiental, emitido pelo funcionamento de elevadores agrícolas, enquanto componentes do maquinário de beneficiamento de
sementes no interior de uma Unidade de Beneficiamento de Grãos e Sementes.

![](Imagens/I1.jpg "Local")

O entendimento do comportamento físico do agente ( é necessário para estabelecer as medidas de proteção individual e coletiva em unidades de produção rural
ou agroindustrial Portanto, monitorar a emissão de ruído pode ajudar na adoção de medidas que eliminem ou neutralizem o ruído emitido pelo conjunto de máquinas e equipamentos destinados ao beneficiamento ou processamento de produtos
agroindustriais.

![](Imagens/I3.jpg "Riscos")

-
Visualizando o mapa de ruído podemos concluir que toda a área da UBS de referência está acima do nível de segurança acústica, determinado pela NR 15, igual ou inferior a
85dBA, logo o uso de EPI e expressamente necessário em todos os locais.

![](Imagens/I4.jpg "Mapa Colorimétrico")